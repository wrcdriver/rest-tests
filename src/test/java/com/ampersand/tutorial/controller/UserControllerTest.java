package com.ampersand.tutorial.controller;

import com.ampersand.tutorial.RestTestsApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * Simple test for a rest service
 * @author Eduardo Laguna
 * @since 14-02-2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestTestsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturn200WhenSendingRequestWithPathParameter() throws Exception {
        this.mockMvc.perform(get("/user/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber()).andExpect(jsonPath("$.name").isString());
    }

    @Test
    public void shouldReturn404WhenSendingRequestWithNoPathParameter() throws Exception {
        this.mockMvc.perform(get("/user/")).andDo(print()).andExpect(status().isNotFound());
    }
}