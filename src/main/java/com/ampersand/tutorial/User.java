package com.ampersand.tutorial;

/**
 * Defines the user of the application
 * @author Eduardo Laguna
 * @since 11-02-2017.
 */
public class User {

    private long id;

    private String name;

    private String lastname;

    private String alias;

    private String password;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", alias='").append(alias).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
