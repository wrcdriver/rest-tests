package com.ampersand.tutorial.controller;

import com.ampersand.tutorial.User;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

/**
 * Handles all user information
 * @author Eduardo Laguna
 * @since 11-02-2017.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody User getUserInfo(@PathVariable long id) {
        User u = new User();
        u.setId(1L);
        u.setAlias("wando");
        u.setName("Eduardo");
        u.setLastname("Laguna");
        return u;
    }
}
